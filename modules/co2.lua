-- First, we'll create a function that will handle the url requests
local function handleRequest(status, body, headers)
  local result = body
  
  -- We'll update the menubar with the result
  if c_menu == nil then
      c_menu = hs.menubar.new()
      c_menu:setClickCallback(makeRequest)
  end
  -- if string.sub(result, -3) == "ppm" then
  if string.match(result, "^%d") then
    c_menu:setTitle(result)
  else
    c_menu:setTitle("🤷🏻‍♂️")
  end
  
end

-- Now, we'll create a function that will make the url request
local function makeRequest()
  hs.http.asyncGet("http://192.168.1.53:1880/co2", nil, handleRequest)
end

makeRequest()
-- We'll add a timer that will make the request every 60 seconds
c_timer = hs.timer.doEvery(30, makeRequest)
c_timer:start()

return {
  init = module_init
}
