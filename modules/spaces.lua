-- function showDesktopNumberInMenuBar()
-- local menubar = hs.menubar.new()
-- local spaceWatcher = hs.spaces.watcher.new(function(spaceNumber)
-- menubar:setTitle(spaceNumber .. '')
-- end)
-- spaceWatcher:start()
-- end

spaces = require("hs._asm.undocumented.spaces")


-- update display
local function spaces_update_display(sn)
  local str = string.format (sn)
  local styled_str = hs.styledtext.new(str, {font={
                                              name="Helvetica Neue",
                                              size=14
                                            }, 
                                            paragraphStyle={
                                              alignment="left"
                                            }, 
                                            baselineOffset=-0.5
                                            })
  spaces_menu:setTitle(styled_str)
end



local function spaces_create_menu()
  if spaces_menu == nil then
    spaces_menu = hs.menubar.new()
  end
end
spaces_create_menu()


-- function spaced()
local spaceWatcher = hs.spaces.watcher.new(function(spaceNumber)
  num = spaces.activeSpace()
  if(num == 1)
    then sn = "Blonde3"
  elseif(num == 3)
    then sn = "Storyful"
  elseif(num == 4)
    then sn = "YourGrocer"
  elseif(num == 5)
    then sn = "StartupVic"
  else
    sn = "Unknown: "..num
  end
  if(num<7)
    then hs.alert.show(sn)
  end
  spaces_update_display(sn)
end)
spaceWatcher:start()
-- end



return {
  init = module_init
}