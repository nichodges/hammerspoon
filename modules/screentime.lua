local function closeApp(appName)
    local app = hs.application.find(appName)
    if app then
        app:kill()
    end
end

local function isRunning()
    n1 = hs.notify.new(function() closeApp("Slack") end, {
        title="Screentime", 
        informativeText="Slack is open!", 
        soundName="default",
        actionButtonTitle="Close it! 🧨",
          })
    n1:alwaysPresent(true)
    n1:autoWithdraw(false)
    n1:hasActionButton(true)
    n1:withdrawAfter(170)
    n1:send()
end

-- Coolo
local function isAppRunning(appName)
    local apps = hs.application.runningApplications()
    for i = 1, #apps do
        if apps[i]:name() == appName then
            -- return true
            isRunning()
        end
    end
end

-- We'll add a timer that will make the request every 60 seconds
s_timer = hs.timer.doEvery(5, function() isAppRunning("Slack") end)
s_timer:start()

return {
    init = module_init
  }
  