# Hammerspoon Config
## Example Configs
https://github.com/Hammerspoon/hammerspoon/wiki/Sample-Configurations

Cofig loading is based on: https://github.com/tstirrat/hammerspoon-config

## TODO
- [x] Make it go through all windows and centre/scale for big screen
- [x] Spectacle Clone: https://github.com/miromannino/hammerspoon-config
- [x] Pomodoro
- [x] Setup config files correctly
- [x] Centre a window
- [ ] Set Pomodoro to remove previous notification
- [ ] All window resize based on WiFi network (ie. Home vs. Work)
- [ ] Something that takes a URL in the clipboard, and converts to a shortened URL (Bit.ly or something less tracky)
