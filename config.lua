local config = {}

config.modules = {
    "auto-reload",
    "pomodoro",
    "spectacles",
    "co2",
    "screentime",
    -- "spaces"
}

return config